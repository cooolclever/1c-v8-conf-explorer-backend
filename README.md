# Usage

1. Ensure you Node.js installed. [See](https://nodejs.org/en/download/)

2. Clone this repo by running the command - `git clone cooolclever@bitbucket.org:cooolclever/1c-v8-conf-explorer-backend.git`

3. Navigate to the directory where the repo is cloned to. (e.g `cd 1c-v8-conf-explorer-backend`)

4. Run `npm install` | `yarn` to install all the dependencies.

5. Start the application locally by running `npm start` | `yarn start`

# What is it?

![](screenshot.png)



# License

[The MIT License](LICENSE).
