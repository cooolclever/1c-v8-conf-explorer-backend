// tslint:disable-next-line:class-name
export interface V8ConfigRow {
    FileName: string;
    BinaryData: any;
    Modified?: Date;
    Created?: Date;
    DataSize?: number;
}
