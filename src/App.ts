import * as express from "express";
import * as sql from "mssql";
import { AddressInfo } from "net";
import * as zlib from "zlib";
import { Config } from "../config/config";
import { MetaName } from "./MetaName";
import { SQLinstance } from "./SQLinstance";
import { V8ConfigRow } from "./V8ConfigRow";
export class App {
  public static async executeQuery<T>(
    q: string,
    instance: SQLinstance
  ): Promise<sql.IRecordSet<T>> {
    const pool = await new sql.ConnectionPool(instance.getConfig());
    const connection = await pool.connect();
    const request = await connection.request();
    const result = await request.query(q);
    return result.recordset;
  }
  public port: number;
  private app: express.Application;
  private metaNames: MetaName[];
  private mainBaseConfig: sql.config;
  public init = async () => {
    this.port = parseInt(process.env.PORT, 10) || 8182;
    try {
      await this.startServer();
    } catch (e) {
      this.port++;
      await this.startServer();
    }
    return 1;
  };

  private startServer() {
    this.app = express();

    const server = this.app.listen(this.port, () => {
      const addressInfo: AddressInfo = server.address() as AddressInfo;
      const host = addressInfo.address;
      console.log("app listening at http://%s:%s", host, this.port);
    });
    this.app.get("/module/:instance?/:filename?", function (req, res) {
      res.header("Access-Control-Allow-Origin", "*");
      // console.log(req.params);
      const server: string = req.param("instance") || Config.DEFAULT_INSTANCE;
      const database: string =
        (req.param("database") as string) || Config.DEFAULT_INSTANCE;
      const user: string =
        (req.param("user") as string) || Config.DEFAULT_LOGIN;
      const password: string =
        (req.param("password") as string) || Config.DEFAULT_PWD;
      const filename: string =
        req.param("filename") || "f77d1108-32b5-478f-b2dc-6ff866557b96.0";
      const raw: boolean = req.param("raw") !== undefined;
      const sqlConfig: sql.config = {
        user,
        password,
        server,
        database,
        pool: {
          max: 150,
          min: 4,
          idleTimeoutMillis: 30000,
        },
      };
      const pool = new sql.ConnectionPool(sqlConfig);

      pool.connect(function () {
        const request = pool.request();
        // var query = "SELECT * FROM sup_kkm_ext.dbo.ГруппыОшибокОС ORDER BY КоличествоИБ  DESC"
        const query = `SELECT BinaryData, FileName FROM Config WHERE FileName = '${filename}'`;

        console.time(server);
        console.timeLog(server, server + ": " + query);
        request.query(query, function (err, recordset) {
          if (err) {
            console.error("     " + server + ": error " + err);
            console.timeLog(server, server + ": " + err);
            console.timeEnd(server);
            req.statusCode = 502;
            res.status(400);
            res.end(`error ${err},  name ${filename} not found`);
            return 502;
          }
          // pool.close()
          if (!recordset || recordset.recordset.length === 0) {
            req.statusCode = 502;
            res.status(400);
            res.end(`name ${filename} not found`);

            console.timeLog(server, server + ": not found");
            console.timeEnd(server);
            return 502;
          }

          res.contentType("application\\json");
          const row: V8ConfigRow = recordset.recordset[0] as V8ConfigRow;
          const binaryData = row.BinaryData;
          const pureBody = zlib.inflateRawSync(binaryData).toString();
          if (raw) {
            res.end(pureBody);
          } else {
            // const huinya = "0004ff14 0004ff14 7fffffff"
            const huinya = "7fffffff";
            const i =
              pureBody.substr(0, 800).lastIndexOf(huinya) + huinya.length + 3;
            const result = pureBody.substr(i);
            // console.log(res.getHeaders());
            res.end(result); // Result in JSON format
          }
          console.timeEnd(server);
          pool.close();
        });
      });
    }); // .get
    this.app.get("/metadata/list", async (req, res) => {
      res.header("Access-Control-Allow-Origin", "*");
      // console.log(req.url, req.params);
      const instance: string = req.param("instance") || Config.DEFAULT_INSTANCE;
      const database: string =
        (req.param("database") as string) || Config.DEFAULT_INSTANCE;
      const user: string =
        (req.param("user") as string) || Config.DEFAULT_LOGIN;
      const password: string =
        (req.param("password") as string) || Config.DEFAULT_PWD;
      const onlyWithDataBindings: number =
        req.param("onlyWithDataBindings") === undefined ? 0 : 1;
      const sqlConfig: sql.config = {
        user,
        password,
        server: instance,
        database,
        pool: {
          max: 10,
          min: 4,
          idleTimeoutMillis: 30000,
        },
      };
      let recordset;
      const pool = new sql.ConnectionPool(sqlConfig);
      const connect = await pool.connect();
      const request = await pool.request();
      const query: string =
        "SELECT c.FileName,(BinaryData) BinaryData FROM config c WHERE len(c.FileName) = 36";

      try {
        recordset = await request.query(query);
      } catch (err) {
        // console.error(err);
      }

      const rows: V8ConfigRow[] = recordset.recordset as V8ConfigRow[];

      const md: MetaName[] = [];
      rows.forEach((row) => {
        const binaryData = row.BinaryData;
        const pureBody = zlib.inflateRawSync(binaryData).toString();
        let typeOfFile: string;
        const forms = this.getForms(pureBody, rows);
        // log(forms);
        if (pureBody.match(/.*cb289fb6d65e.*/)) {
          typeOfFile = "Документ";
        } else if (pureBody.match(/.*fdf816d2-1ead-11d5-b975-0050bae0a95d.*/)) {
          typeOfFile = "Справочник";
        } else if (pureBody.match(/.*2bcef0d1-0981-11d6-b9b8-0050bae0a9.*/)) {
          typeOfFile = "Обработка";
        } else if (pureBody.match(/{1,\r\n{12,\r\n{1,\r\n.*/g)) {
          typeOfFile = "ОбщийМодуль";
        }

        const fileName = row.FileName;
        const body: string = pureBody;
        let name: string = "";
        const type: string = typeOfFile;
        if (typeOfFile) {
          const matches: RegExpExecArray = /{0,0.+?"([А-Яа-я_a-zA-Z\d]*).*/.exec(
            pureBody
          );
          name = matches[1];
          // res.write(name + String.fromCharCode(10))
        } else {
          const matches: RegExpExecArray = /{0,0.+?"([А-Яа-я_a-zA-Z\d]*).*/.exec(
            pureBody
          );
          name = matches[1];
          // res.write("           " + matches + String.fromCharCode(10))
        }
        const metaRow: MetaName = { fileName, name, body, type, forms };
        md.push(metaRow);
      });
      md.sort((a: MetaName, b: MetaName): number => {
        if (a.name > b.name) {
          return 1;
        }
        return -1;
      });
      const mdThree = this.getThree(md);

      res.end(
        JSON.stringify(mdThree, (k, v) => {
          if (k === "body") {
            v = NaN;
          } else {
            return v;
          }
        })
      );
    }); // .get

    this.app.get("/files", async (req, res) => {
      res.header("Access-Control-Allow-Origin", "*");
      // console.log(req.url, req.params);
      const instance: string = req.param("instance") || Config.DEFAULT_INSTANCE;
      const database: string =
        (req.param("database") as string) || Config.DEFAULT_INSTANCE;
      const user: string =
        (req.param("user") as string) || Config.DEFAULT_LOGIN;
      const password: string =
        (req.param("password") as string) || Config.DEFAULT_PWD;
      const onlyWithDataBindings: number =
        req.param("onlyWithDataBindings") === undefined ? 0 : 1;
      const sqlConfig: sql.config = {
        user,
        password,
        server: instance,
        database,
        pool: {
          max: 10,
          min: 4,
          idleTimeoutMillis: 30000,
        },
      };
      let recordset;
      const pool = new sql.ConnectionPool(sqlConfig);
      const connect = await pool.connect();
      const request = await pool.request();
      const query: string =
        "SELECT c.FileName,(BinaryData) BinaryData FROM config c WHERE len(c.FileName) = 36";

      try {
        recordset = await request.query(query);
      } catch (err) {
        // console.error(err);
      }

      const rows: V8ConfigRow[] = recordset.recordset as V8ConfigRow[];

      const md: MetaName[] = [];
      rows.forEach((row) => {
        const binaryData = row.BinaryData;
        const pureBody = zlib.inflateRawSync(binaryData).toString();
        let typeOfFile: string;
        if (pureBody.match(/.*cb289fb6d65e.*/)) {
          typeOfFile = "Документ";
        } else if (pureBody.match(/.*fdf816d2-1ead-11d5-b975-0050bae0a95d.*/)) {
          typeOfFile = "Справочник";
        } else if (pureBody.match(/.*2bcef0d1-0981-11d6-b9b8-0050bae0a9.*/)) {
          typeOfFile = "Обработка";
        }

        const fileName = row.FileName;
        const body: string = pureBody;
        let name: string = "";
        const type: string = typeOfFile;
        if (typeOfFile) {
          const matches: RegExpExecArray = /{0,0.+?"([А-Яа-я_a-zA-Z\d]*).*/.exec(
            pureBody
          );
          name = matches[1];
          // res.write(name + String.fromCharCode(10))
        } else {
          const matches: RegExpExecArray = /{0,0.+?"([А-Яа-я_a-zA-Z\d]*).*/.exec(
            pureBody
          );
          name = "." + matches[1];
          // res.write("           " + matches + String.fromCharCode(10))
        }
        const metaRow: MetaName = { fileName, name, body, type };
        md.push(metaRow);
      });
      md.sort((a: MetaName, b: MetaName): number => (a.name > b.name ? 1 : 0));
      res.end(
        JSON.stringify(md, (k, v) => {
          if (k === "body") {
            v = NaN;
          } else {
            return v;
          }
        })
      );
    });
  }
  private getThree(md: MetaName[]): MetaName[] {
    const types = [undefined];
    const three: MetaName[] = [];
    md.forEach((v) => {
      if (!types.includes(v.type)) {
        types.push(v.type);
        const mn: MetaName = {
          fileName: v.type,
          type: v.type,
          name: v.type,
          body: "",
          children: [],
        };
        three.push(mn);
      }
    });

    three.forEach((el) => {
      el.children = md.filter((v) => v.type === el.type) || [];
      el.children.sort((a, b) => {
        return a.name > b.name ? 1 : 0;
      });
      el.children.forEach((metaName) => {
        metaName.parent = el.fileName;
        if (metaName.forms === undefined) {
          return;
        }
        const forms: MetaName[] = [];
        metaName.forms.forEach((formFileName) => {
          forms.push(
            md.find((ths, value) => {
              return ths.fileName === formFileName;
            })
          );
        });
        // forms.forEach(element => {
        //   element.parent = metaName;
        // });
        metaName.children = forms;
      });
    });
    return three;
  }
  private getForms(body: string, metadata?: V8ConfigRow[]): string[] {
    let strarray = body.split("fb880e93-47d7-4127-9357-a20e69c17545");
    if (strarray.length === 1) {
      strarray = body.split("d5b0e5ed-256d-401c-9c36-f630cafd8a62");
    }
    if (strarray.length === 1) {
      strarray = body.split("fdf816d2-1ead-11d5-b975-0050bae0a95d");
    }
    if (strarray.length > 1) {
      let str = strarray[1];
      const i = str.indexOf("}");
      const result = [];
      str = str.substr(3, i - 3);

      const guids = str.split(",");
      return guids;
    }

  }
}
