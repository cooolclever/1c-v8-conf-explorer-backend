import * as sql from "mssql";
import { } from "../config/config";
import { App } from "./App";
export class SQLinstance {
    public static readonly MAIN_INSTANCE: SQLinstance = new SQLinstance("sup_kkm")
    public static getById(id: number) {
        const q: string = `select * from ce.dbo.instances where id = {id}`
        App.executeQuery(q, this.MAIN_INSTANCE);
    }
    private id: number
    private instanceName: string
    private login: string
    private pwd: string
    public constructor(instanceName: string, login: string = "sa", pwd: string = "ser09l") {
        this.instanceName = instanceName
        this.login = instanceName
        this.pwd = instanceName
    }

    public getConfig() {
        const sqlConfig: sql.config = {
            user: this.login,
            password: this.pwd,
            server: this.instanceName,
            database: "",
            pool: {
                max: 10,
                min: 4,
                idleTimeoutMillis: 30000,
            },
        }
        return sqlConfig
    }
}
