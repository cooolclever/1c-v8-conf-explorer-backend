export interface MetaName {
  children?: MetaName[];
  fileName: string;
  name: string;
  type?: string;
  body: string;
  forms?: string[];
  parent?: string;
}
