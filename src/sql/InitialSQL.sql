IF NOT EXISTS (SELECT * FROM sys.databases WHERE name = 'ce')
	CREATE DATABASE ce

BEGIN TRY
	exec sp_executesql N'SELECT * from ce.dbo.instances'
END TRY
BEGIN CATCH
	CREATE TABLE ce.dbo.instances (
		id int IDENTITY,
		instanceName varchar(max),
		login varchar(max) DEFAULT 'sa',
		pwd varchar(max) DEFAULT 'ser09l'
	)
END CATCH

